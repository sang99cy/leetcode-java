package stackandqueue;

import com.sun.org.apache.bcel.internal.generic.ATHROW;

class MyArrayStack implements IStackQueue {

    private int[] array;
    private int SIZE;
    private int topIndex;

    MyArrayStack(int size){
        SIZE = size;
        array = new int[SIZE];
        topIndex = -1;// stack dang rong
    }


    public boolean push(int value) {
        if(!isFull()){
            topIndex++;
            array[topIndex] = value;
            return true;
        }
        return false;
    }

    public int pop() {
        if (!isEmpty()){
            int value =  array[topIndex];
            topIndex--;
            return value;
        }else {
            throw new RuntimeException("stack is empty");
        }
    }

    public boolean isFull() {
        return topIndex == SIZE - 1;
    }

    public boolean isEmpty() {
        return topIndex < 0;
    }

    public void show() {
       if(isEmpty()){
           System.out.println("Stack is Empty!");
       }else{
           for(int i= 0;i <= topIndex;i++){
               System.out.println(array[i] + " ");
           }
       }
    }
}
