package stackandqueue;

import java.util.Stack;

public class StackQueueJava {
    public static void main(String[] args) {
        Stack<Integer> myStack = new Stack<Integer>();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        for (Integer i : myStack){
            System.out.println(i);
        }
        System.out.println(myStack.peek());
        for (Integer i : myStack){
            System.out.println(i);
        }
    }
}
