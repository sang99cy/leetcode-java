package stackandqueue;

import java.util.ArrayList;

public class MyLinkedListStack implements IStackQueue {

    private class Node{
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    Node topNode;
    public MyLinkedListStack() {
        topNode = null;
    }

    public boolean push(int value) {
        if(!isFull()){
            //them 1 p tu va dau cua link list
            Node newNode = new Node(value);
            newNode.next = topNode;
            topNode = newNode;
            return true;
        }
        return false;
    }

    public int pop() {
        if(isEmpty())
            return -1;
        int value = topNode.value;
        topNode = topNode.next;
        return value;
    }

    public boolean isFull() {
        //khi nao full tuy vao chuong trinh cap phat
        return false;
    }

    public boolean isEmpty() {
        return topNode == null;
    }

    public void show() {
        if(isEmpty()){
            System.out.println("stack is Empty!");
        }
        Node temp = topNode;
        ArrayList<Integer> temList = new ArrayList<Integer>();
        while (temp != null){
//            System.out.print(temp.value+" ");
            temList.add(temp.value);
            temp = temp.next;
        }
        for (int i=temList.size()-1;i >=0;i--){
            System.out.println(temList.get(i)+" ");
        }
        System.out.println();
    }
}
