package stackandqueue;

import com.sun.org.apache.bcel.internal.generic.FADD;

public class MyLinkListQueue implements IStackQueue {

    public class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    Node headNode, tailNode;

    public MyLinkListQueue() {
        headNode = tailNode = null;
    }

    public boolean push(int value) {
        if (isFull()) {
            return false;
        }
        Node newNode = new Node(value);
        if(isEmpty()){
            headNode = tailNode = newNode;
        }else {
            tailNode.next = newNode;
            tailNode = newNode;
        }
        return true;
    }

    public int pop() {
        if(isEmpty()){
            return -1;
        }
        int value = headNode.value;
        if(headNode == tailNode){
            headNode = tailNode = null;
        }else {
            headNode = headNode.next;
        }
        return -1;
    }

    public boolean isFull() {
        return false;
    }

    public boolean isEmpty() {
        return (headNode == null && tailNode == null);
    }

    public void show() {
        if(isEmpty()){
            System.out.println("Queue is empty");
            return;
        }
        Node curNode = headNode;
        while (curNode != null){
            System.out.println(curNode.value + "");
            curNode = curNode.next;
        }
        System.out.println();
    }

}
