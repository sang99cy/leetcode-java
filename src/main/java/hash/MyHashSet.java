package hash;

import lombok.var;

import java.util.ArrayList;
/*ap dung cau truc nay tren leet code  : bai 705*/
/*bai 217: Contains Duplicate */
public class MyHashSet {
    private final int SIZE = 1000;
    private ArrayList<Integer> myBuckets[];//mang hai chieu

    public MyHashSet() {
        myBuckets = new ArrayList[SIZE];
        for (int i = 0; i < myBuckets.length; i++) {
            myBuckets[i] = new ArrayList<Integer>();
        }
    }

    //return hashValue -> la index cua 1 cai bat ki
    private int hashFunction(int key) {
        return key % SIZE;
    }

    public void add(int key) {
        int hashValueIndex = hashFunction(key);
        var bucket = myBuckets[hashValueIndex];
        int keyIndex = bucket.indexOf(key);
        if (keyIndex < 0) {
            bucket.add(key);
        }
    }

    public void remove(int key) {
        int hasValueIndex = hashFunction(key);
        var bucket = myBuckets[hasValueIndex];
        int keyIndex = bucket.indexOf(key);
        if (keyIndex >= 0) {
            bucket.remove(keyIndex);
        }
        int x = 1;
    }

    public boolean contains(int key) {
        int hashValueIndex = hashFunction(key);
        var bucket = myBuckets[hashValueIndex];
        int keyIndex = bucket.indexOf(key);
        return keyIndex >= 0;
    }

    public static void main(String[] args) {
        MyHashSet myHashSet = new MyHashSet();
        myHashSet.add(1);
        myHashSet.add(1);
        myHashSet.add(2);
        System.out.println("myHashSet,contains(1):" + myHashSet.contains(1));
        myHashSet.remove(1);
        System.out.println("myHashSet,contains(1):" + myHashSet.contains(1));
    }
}
