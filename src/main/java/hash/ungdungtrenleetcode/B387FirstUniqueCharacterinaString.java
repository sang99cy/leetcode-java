package hash.ungdungtrenleetcode;

import java.util.HashMap;
import java.util.Map;

/*tim ki tu dau tien chi xuat hien 1 lan va tra ve index cua no*/
public class B387FirstUniqueCharacterinaString {

    public int firstUniqChar(String s) {
        Map<Character, Integer> myCountMap = new HashMap<Character, Integer>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (myCountMap.containsKey(c) == false) {
                myCountMap.put(c, 1);
            }else {
                myCountMap.put(c,myCountMap.get(c) + 1);
            }
        }

        for(int i=0;i < s.length();i++){
            char c = s.charAt(i);
            if(myCountMap.get(c) == 1){
                return i;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        String a = "leetcode";
        B387FirstUniqueCharacterinaString test = new B387FirstUniqueCharacterinaString();
        int k = test.firstUniqChar(a);
        System.out.println(k);
    }
}
