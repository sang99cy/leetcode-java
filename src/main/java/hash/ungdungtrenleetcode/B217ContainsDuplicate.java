package hash.ungdungtrenleetcode;

import java.util.HashSet;
import java.util.Set;

public class B217ContainsDuplicate {

    public boolean containsDuplicate(int[] nums) {
        Set<Integer> mySet = new HashSet<Integer>();
        for (int n : nums){
            if(mySet.contains(n) == true){// da ton tai tring my set
                return true;
            }else {
                mySet.add(n);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] nums = {1,1,1,3,3,4,3,2,4,2};
        B217ContainsDuplicate test = new B217ContainsDuplicate();
        boolean kt = test.containsDuplicate(nums);
        System.out.println(kt);
    }
}
