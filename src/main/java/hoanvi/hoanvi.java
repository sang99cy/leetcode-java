package hoanvi;

public class hoanvi {

    public static void hoanvi1(int a, int b) {
        System.out.println("a = %d, b = %d " + a + "," + b);
        // Swap a and b:
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("\na = %d, b = %d" + a + "," + b);
        int x = 0;
    }

    public static void hoanvi2(int a,int b){
        System.out.println("a = %d, b = %d " + a + "," + b);
        // Swap a and b:
        a = a * b;
        b = a / b;
        a = a / b;
        System.out.println("\na = %d, b = %d" + a + "," + b);
        int x = 0;
    }

    public static void hoanvi3(int a,int b){
        System.out.println("a = %d, b = %d " + a + "," + b);
        // Swap a and b:
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("\na = %d, b = %d" + a + "," + b);
        int x = 0;
    }

    public static void main(String[] args) {
        hoanvi1(2,4);
        hoanvi2(2,4);
        hoanvi3(2,4);
    }
}
