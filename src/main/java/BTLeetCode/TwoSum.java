package BTLeetCode;

import java.util.Date;
import java.util.TimeZone;

public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        int results[] = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    results[0] = i;
                    results[1] = j;
                }
            }
        }
        return results;
    }

    public static void main(String[] args) {
        TwoSum test = new TwoSum();
        //test case 1: nums = [2,7,11,15], target = 9
        int nums1[] = {2, 7, 11, 15};
        int[] results1 = test.twoSum(nums1,9);

        //test case 2:nums = [3,2,4], target = 6
        int nums2[] = {3,2,4};
        int[] results2 = test.twoSum(nums2,6);

        //test case 3:nums = [3,3], target = 6
        int nums3[] = {2, 7, 11, 15};
    }
}
