package recursion;

public class TinhGiaiThua {

    public int giaiThua(int n) {
        // bài toán cơ sở
        if (n == 0)
            return 1;
        // công thức quy nap;
        int result = n * giaiThua(n - 1);
        return result;
    }

    public static void main(String[] args) {
        TinhGiaiThua gt = new TinhGiaiThua();
        System.out.println("3! = " + gt.giaiThua(3));
        System.out.println("2! = " + gt.giaiThua(2));
        System.out.println("1! = " + gt.giaiThua(1));
    }
}
