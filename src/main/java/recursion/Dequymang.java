package recursion;

public class Dequymang {
    /*1.In ra cac phan tu trong mang bang phuong phap de quy thay vi dung for*/
    /*in ra cac phan tu tu 0 -> n-1*/
    void printElement(int[] arr, int index) {
        // bai toan co so
        if (index < 0 | index >= arr.length)
            return;
        // cong thuc quy nap
        System.out.println(arr[index]);
        printElement(arr, index + 1);
    }

    /*C1: int ra cac phan tu n -1 den 0*/
    void printElemnet1(int arr[], int index) {
        // bai toan co so
        if (index < 0 | index >= arr.length)
            return;
        //cong thuc quy nap
        System.out.println(arr[index]);
        printElemnet1(arr, index - 1);
    }
    /*C2: int ra cac phan tu n -1 den 0*/
    void printElemnet2(int arr[], int index) {
        // bai toan co so
        if (index < 0 | index >= arr.length)
            return;
        // cong thuc quy nap
        printElement(arr, index + 1);
        System.out.println(arr[index]);
    }

    public static void main(String[] args) {
        Dequymang dequymang = new Dequymang();
        int arr[] = {1, 2, 3, 13, 6, 7, 8, 9, 2};
        //dequymang.printElement(arr, 0);
        //dequymang.printElemnet1(arr,8);
        dequymang.printElemnet2(arr,0);
    }
}
