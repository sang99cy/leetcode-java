package recursion;

public class TinhsoFibonacy {

    public int tinhFibonacy(int n) {
        if (n <= 2)
            return 1;
        System.out.println("Can tinh F[" + n + "]");
        int result = tinhFibonacy(n - 1) + tinhFibonacy(n - 2);
        return result;
    }

    /* de quy co nho*/
    static int[] F = new int[1000];
    public static int Fibo2(int n) {
        if (F[n] != 0)
            return F[n];
        System.out.println("Can tinh F[" + n + "]");
        if (n <= 2) {
            F[1] = 1;
            F[2] = 1;
            return 1;
        }
        F[n] = Fibo2(n - 1) + Fibo2(n - 2);
        return F[n];

    }

    public static void main(String[] args) {
        TinhsoFibonacy tinhsoFibonacy = new TinhsoFibonacy();
       /* System.out.println("finbonacci cua 1 : " + tinhsoFibonacy.tinhFibonacy(1));
        System.out.println("finbonacci cua 2 : " + tinhsoFibonacy.tinhFibonacy(2));*/
        //System.out.println("finbonacci cua 10 : " + tinhsoFibonacy.tinhFibonacy(10));
        System.out.println("finbonacci cua 10 : " + tinhsoFibonacy.Fibo2(10));
    }
}
