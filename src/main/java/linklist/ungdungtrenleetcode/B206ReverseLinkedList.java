package linklist.ungdungtrenleetcode;

import java.util.LinkedList;


public class B206ReverseLinkedList {

    public static class ListNode {
        int val;
        ListNode next;

        public ListNode() {
        }

        public ListNode(int val) {
            this.val = val;
        }

        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static ListNode reverseList(ListNode head) {
        ListNode curNode = head;
        while (curNode != null && curNode.next != null) {
            ListNode nextNode = curNode.next;
            curNode.next = nextNode.next;
            nextNode.next = head;
            head = nextNode;
        }
        return head;
    }

    //C2: ap dung de quy
    public static ListNode reverseList1(ListNode head) {
        //TH CS
        if (head == null)
            return null;
        ListNode nexNode = head.next;
        // TH 1 node
        if (nexNode == null)
            return head;
        // TH TQ 2 node tro len
        ListNode newHead =  reverseList1(nexNode);
        nexNode.next = head;
        head.next = null;
        return newHead;
    }

    public static void printLinkedList(ListNode head) {
        while (head != null) {
            System.out.printf("%d", head.val);
            head = head.next;
        }
        System.out.println();
    }


    public static void main(String[] args) {
        ListNode a1 = new ListNode(1);
        ListNode a2 = new ListNode(2);
        ListNode a3 = new ListNode(3);
        a1.next = a2;
        a2.next = a3;
        printLinkedList(a1);
        ListNode newList = reverseList1(a1);
        printLinkedList(newList);

        /*su dung thu vien collection trong java de giai quyet baiu toan*/
    }
}
