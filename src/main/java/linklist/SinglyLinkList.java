package linklist;
/*xay dung 1 so ham co ban cho linklist:
* 1.them dau ,them cuoi, them bat ki
* 2. xoa dau , xoa cuoi, xoa bat ki*/
public class SinglyLinkList {

    public static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    public static void printLinkList(Node head){
        if(head == null){
            System.out.println("List is empty");
        }else {
            Node temp = head;
            while (temp != null){
                System.out.print(temp.value);
                temp = temp.next;
                if(temp != null){
                    System.out.print("->");
                }else {
                    System.out.println("");
                }
            }
        }
    }

    public static Node addToHead(Node headNode,int value){
        Node newNode = new Node(value);

        if(headNode != null){
            newNode.next = headNode;
        }
        return newNode;
    }

    public static Node addToTail(Node headNode,int value){
        Node newNode =  new Node(value);
        if(headNode == null){
            return newNode;
        }else {
           // B1. Xác định lastNode
            Node lastNode = headNode;
            while (lastNode.next != null){
                lastNode = lastNode.next;
            }
            //gán next cho lastNode = newNode
            lastNode.next = newNode;
        }
        return headNode;
    }

    public static Node addToIndex(Node headNode,int value,int index){
        Node newNode = new Node(value);
        if(index == 0){
            return addToHead(headNode,value);
        }else {
            //B1: Tìm vị trí cần thêm
            Node currentNode = headNode;
            int count = 0;
            while (currentNode != null){
                count ++;
                if(count == index){
                    //thuc hien add
                    newNode.next = currentNode.next;
                    currentNode.next = newNode;
                    break;
                }
                currentNode = currentNode.next;
            }
        }
        return headNode;
    }

    //xoa dau
    public static Node removeAtHead(Node headNode){
        if(headNode != null){
            return headNode.next;
        }
        return headNode;
    }

    //xoa cuoi
    public static Node removeAtTail(Node headNode) {
        if(headNode == null)
            return null;

        //B1. Xac dinh last va previous
        Node lastNode = headNode;
        Node prevNode = null;

        while(lastNode.next != null){ // lastNode.next = null
            prevNode = lastNode;
            lastNode = lastNode.next;
        }

        if(prevNode == null){
            return null;
        }else{
            prevNode.next = lastNode.next;
        }

        return headNode;
    }

    public static Node removeAtIndex(Node headNode,int index){
        if(headNode == null || index < 0)
            return null;
        if(index == 0)
            return removeAtHead(headNode);
        Node curNode = headNode;
        Node preNode = null;
        int count = 0;
        boolean bIsFound = false;
        while (curNode != null){
            if(count == index){
                //remove currentNode
                bIsFound = true;
                break;
            }
            preNode = curNode;
            curNode = curNode.next;
            count ++;
        }
        // remove currentNode
        if(bIsFound == true){
            if(preNode == null){// curNode is lastNode
                return null;
            }else {
                if (curNode != null){
                    preNode.next = curNode.next;
                }
            }
        }

        return headNode;
    }

    public static void main(String[] args) {
        Node n1 =new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);

        n1.next = n2;
        n2.next = n3;

        printLinkList(n1);
        n1 =  removeAtIndex(n1,0);
        n1 =  removeAtIndex(n1,1);
        n1 = removeAtIndex(n1,0);
        printLinkList(n1);
    }
}
